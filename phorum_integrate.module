<?php


/**
 * Implements module_help hook
 *
 * @param string $section
 * @return string
 */
function phorum_integrate_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Integrates a phorum message board.');
  }
  return '';
}

/**
 * Implements module_perm hook
 *
 * @return array
 */
function phorum_integrate_perm() {
  return array('access phorum', 'administer phorum');
}

/**
 * Implements module_menu hook
 *
 * @param bool $may_cache
 * @return array
 */
function phorum_integrate_menu($may_cache) {
  $items = array();
  if ($may_cache) {
  	$phorum_home = variable_get('phorum_integrate_home', 'phorum');
  	$phorum_url = variable_get('phorum_integrate_url', 'discuss');
    $items[] = array(
      'path' => $phorum_url, 'title' => t('discussion area'),
      'callback' => 'phorum_integrate_show_page',
      'access' => user_access('access phorum'),
    );
    $items[] = array(
      'path' => "$phorum_home/admin.php",
      'title' => t('administer phorum'),
      'access' => user_access('administer phorum'),
    );
  }
  return $items;
}

/**
 * Implements module_show_page hook
 *
 * @return string
 */
function phorum_integrate_show_page($page='index') {
	$phorum_home = variable_get('phorum_integrate_home', 'phorum');

	// load the integration base class and our extension of it
  include_once "./$phorum_home/mods/embed_phorum/PhorumConnectorBase.php";
  include_once './modules/phorum_integrate/phorum_connector.php';

  // this global variable is used by the embed_phorum mod to locate
  // our connector object
	global $PHORUM_CONNECTOR;
	$PHORUM_CONNECTOR = new drupalPhorumConnector($page);

	// run phorum embedded - page data is passed to the connector
	// rather than directly displayed
	include("./$phorum_home/mods/embed_phorum/run_phorum.php");

	// get the page data from the connector
  drupal_set_html_head($PHORUM_CONNECTOR->get_element('style'));
  drupal_set_html_head($PHORUM_CONNECTOR->get_element('redirect_meta'));
	drupal_set_html_head($PHORUM_CONNECTOR->get_element('head_data'));
	drupal_set_title($PHORUM_CONNECTOR->get_element('title'));
	return $PHORUM_CONNECTOR->get_element('body_data');
}

/**
 * Implements module_settings hook
 *
 * @return array
 */
function phorum_integrate_settings() {
if (!user_access('access administration pages')) {
    return message_access();
  }
  $form['phorum_integrate_home'] =
	  array(
	  	'#type' => 'textfield',
	  	'#title' => t('Phorum installation directory'),
	  	'#default_value' => variable_get('phorum_integrate_home', 'phorum'),
	  	'#description' => t("The directory where Phorum was installed (relative to the Drupal root)."),
	  	'#maxlength' => '255',
	  	'#size' => '50'
	  );
  $form['phorum_integrate_url'] =
	  array(
	  	'#type' => 'textfield',
	  	'#title' => t('Phorum page url'),
	  	'#default_value' => variable_get('phorum_integrate_url', 'discuss'),
	  	'#description' => t("The Drupal path where the forum will appear."),
	  	'#maxlength' => '255',
	  	'#size' => '50'
	  );
  $form['phorum_integrate_template'] =
	  array(
	  	'#type' => 'textfield',
	  	'#title' => t('Phorum template'),
	  	'#default_value' => variable_get('phorum_integrate_template', 'embed_phorum'),
	  	'#description' => t("The template used to style forum display."),
	  	'#maxlength' => '255',
	  	'#size' => '40'
	  );
  $form['phorum_integrate_db_prefix'] =
	  array(
	  	'#type' => 'textfield',
	  	'#title' => t('Prefix to phorum tables'),
	  	'#default_value' => variable_get('phorum_integrate_db_prefix', 'phorum_'),
	  	'#description' => t("The prefix to the names of the Phorum database tables."),
	  	'#maxlength' => '255',
	  	'#size' => '40'
	  );
  return $form;
}
