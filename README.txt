
Description
-----------

Integrates a Phorum5 message board into a Drupal site.
This is meant as a temporary expedient in cases where the Drupal forum
does not interoperate with other modules (category and organic groups etc).

Installation
------------
1. Extract the Phorum files in a directory immediately below the Drupal root. The
default name for this directory is "phorum".

2. Backup your Drupal database.

3. Configure Phorum to use the same database as Drupal, making sure you set a prefix
on the names for its database tables (default "phorum_"), and follow the standard
instructions.

4. Download the embed_phorum package written by Maurice Makaay from
http://phorum-dev.gitaar.net/packages/embed_phorum-0.0.13.zip and unzip it in your 
Phorum installation's "mod" directory.

5. Go to the Phorum admin page (eg "phorum/admin.php") and enable the embed_phorum
module. After this point any attempt to access the Phorum message boards directly
will produce an error message.

6. Enable this module through Drupal "admin/modules".

7. Check and if necessary update the module settings at "admin/settings/phorum".
By default the phorum will be accessed through the path "discuss", but you can
set a different value if you want.

8. Assign the new privileges 'access phorum' and 'administer phorum' to user roles as 
required. 